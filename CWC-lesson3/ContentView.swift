//
//  ContentView.swift
//  CWC-lesson3
//
//  Created by Maciek Janik on 28/12/2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ZStack{
            
            Color(.systemMint).ignoresSafeArea()
            
            VStack(alignment: .leading, spacing: 20.0) {
                
                Image("tt")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .cornerRadius(35)
                
                HStack {
                    Text("Moje TT")
                        .font(.title).bold()
                    
                    Spacer()
                    
                    VStack{
                        HStack{
//                            Image(systemName: "star.fill")
//                                .foregroundColor(.orange)
//                                .font(.caption)
//                            Image(systemName: "star.fill")
//                                .foregroundColor(.orange)
//                                .font(.caption)
//                            Image(systemName: "star.fill")
//                                .foregroundColor(.orange)
//                                .font(.caption)
//                            Image(systemName: "star.leadinghalf.filled")
//                                .foregroundColor(.orange)
//                                .font(.caption)
//                            Image(systemName: "star")
//                                .foregroundColor(.orange)
//                                .font(.caption)
                            
                            Image(systemName: "star.fill")
                            Image(systemName: "star.fill")
                            Image(systemName: "star.fill")
                            Image(systemName: "star.leadinghalf.filled")
                            Image(systemName: "star")
                        }.foregroundColor(.orange)
                        
                        Text("(Reviews 3456)").font(.caption)
                        
                    }.font(.caption)
                    
                    
                }
                
                Text("Lerum porerum lerum erpo esg rh45 dfae erherh sdsvsrrhse.")
                
                HStack{
                    Spacer()
                    Image(systemName: "binoculars.fill")
                    Image(systemName: "fork.knife")
                }.font(.caption).foregroundColor(.gray)
                
            }
            .padding()
            .background(
                Rectangle()
                    .foregroundColor(.white)
                    .cornerRadius(35)
                    .shadow(radius: 55))
            .padding()
        }
    }
}

#Preview {
    ContentView()
}
