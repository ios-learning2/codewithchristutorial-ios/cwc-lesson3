//
//  CWC_lesson3App.swift
//  CWC-lesson3
//
//  Created by Maciek Janik on 28/12/2023.
//

import SwiftUI

@main
struct CWC_lesson3App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
